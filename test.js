var Animal = {
    move: "run"
};

function Rabbit(name){
    this.name = name;
    this.move = 'jump';
}

Rabbit.prototype = Animal;

var rabbit = new Rabbit('huj');

console.log(rabbit.move);