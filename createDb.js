var mongoose = require('lib/mongoose');
var async = require('async');

async.series([
    open,
    dropDatabase,
    requireModels,
    createUsers
], function(err){
        console.log(arguments);
        mongoose.disconnect();
});

function open(callback){
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback){
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback){
    require('models/user');

    async.each(Object.keys(mongoose.models), function(modelName, callback){
        mongoose.models[modelName].ensureIndexes(callback);
    },callback);
}

function createUsers(callback){
    var users = [
        {username: 'vasil', password: '133'},
        {username: 'janka', password: '1111'},
        {username: 'jakub', password: '1212'}
    ];

    async.each(users, function(userData, callback){
        var user = mongoose.models.User(userData);
        user.save(callback);
    }, callback);
}

