'use strict';
var express = require('express');
var http = require('http');
var path = require('path');
var config  = require('config');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var cookieParser = require('cookie-parser');
//var router = require('router');
var errorHandler = require('errorhandler');
var log = require('lib/log')(module);
var mongoose = require('lib/mongoose');
var HttpError = require('error').HttpError;

var app = express();

app.engine('ejs', require('ejs-locals'));
app.set('port', config.get('port'));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(bodyParser.urlencoded({
    extended: true
}));

if(app.get('env') == 'development'){
    app.use(logger('dev'));
}else{
    app.use(logger('default'));
}
app.use(methodOverride());


app.use(cookieParser());

var sessionStore = require('lib/sessionStore');

app.use(session({
    resave: 'bla',
    saveUninitialized: 'bla',
    secret: config.get("session:secret"),
    key: config.get("session:key"),
    cookie: config.get("session:cookie"),
    store: sessionStore
}));

//app.use(function(req, res, next){
//    req.session.numberOfVisits = req.session.numberOfVisits + 1 || 1;
//    res.send("Visits: "+req.session.numberOfVisits);
//});
app.use(require('middleware/loadUser'));
app.use(require('middleware/sendHttpError'));

//app.use(router);

require('routes')(app);

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());

app.use(function(err, req, res, next) {
    if(typeof  err == 'number'){
        err = new HttpError(err);
    }

    if(err instanceof HttpError) {
        res.sendHttpError(err);
    }else{
        if(app.get('env') == 'development'){
            errorHandler()(err, req, res, next);
        }else{
            log.error(err);
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }
});


var server = http.createServer(app);
server. listen(app.get('port'), function(){
    log.info('Express server listening port '+config.get('port'));
});

var io = require('./socket')(server);
app.set('io', io);